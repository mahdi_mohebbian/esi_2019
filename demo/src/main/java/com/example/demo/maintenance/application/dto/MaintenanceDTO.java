package com.example.demo.maintenance.application.dto;

import com.example.demo.common.application.dto.BusinessPeriodDTO;
import com.example.demo.common.rest.ResourceSupport;
import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import lombok.Data;


@Data
public class MaintenanceDTO extends ResourceSupport {
    Long _id;
    BusinessPeriodDTO maintenancePeriod;
    PlantInventoryEntryDTO plant;
}
