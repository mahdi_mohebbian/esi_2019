package com.example.demo.maintenance.domain;

import com.example.demo.common.domain.BusinessPeriod;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
public class PlantMaintenance {
    @Id
    @GeneratedValue
    Long id;


    @ManyToOne
    PlantInventoryEntry plant;


    @Embedded
    BusinessPeriod maintenancePeriod;

    public static PlantMaintenance of(PlantInventoryEntry entry, BusinessPeriod period) {

        PlantMaintenance pm = new PlantMaintenance();
        pm.plant = entry;
        pm.maintenancePeriod = period;

        return pm;
    }

}
