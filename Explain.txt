﻿I will  explain my approach since I ran out of time to fully implement it.
At this point there should be another package for maintenance tasks handling with exact sub packages as Sales or other sections.
Start preparing the domain model entity of maintenance and its interface repository.
After setting up the DTO and other initializing steps to get maintenance tasks ready I will start working on service and rest controller. 
Before that there would be MaintenanceAssmebler connecting DTO to entities.
In maintenanceService all of the functionalities will be implemented  
including initializing a new maintenance task to a plant from maintenanceDTO as input, accepting a maintenance task, rejecting a maintenance task and getting the list of all maintenance tasks and also getting a specific maintenance task.
After initializing these steps in service section, in the maintenance Controller I will specify urls to each function implemented in service section. 

- For creating a request for maintenance : api/maintenance/task
- For accepting a maintenance request : api/maintenance/task/{id}/accept
- For rejecting a maintenance request : api/maintenance/task/{id}/reject
- For retrieving a maintenance task details : api/maintenance/task/{id}
